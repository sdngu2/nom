# Neaten Offline Moodle (NOM) #
New version currently in development, refer to [original repository.](https://bitbucket.org/sdngu2/nom_legacy)

### Summary ###
NOM is designed for Monash students to be able to download all their resources (lecture slides, materials, etc) from the Moodle site and place these resources into organized folders for offline use. This also downloads the unit's pages whilst removing excess content as well as resources stored in 'folders' (that require you to open another window to view them) so that you can now view everything in one page.

### Requirements ###
* JRE 1.7 or later

### Version ###
* NA