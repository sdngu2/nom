import sdngu2.nom.gui.NOM;

public class Driver {

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NOM().setVisible(true);
            }
        });
    }

}
