package sdngu2.nom.gui;

import sdngu2.nom.gui.components.ProgressCellRender;

public class NOM extends javax.swing.JFrame {

    public NOM() {
        initComponents();
    }

    private void initComponents() {

        spInfo = new javax.swing.JScrollPane();
        taInfo = new javax.swing.JTextArea();
        spDownloads = new javax.swing.JScrollPane();
        tDownloads = new javax.swing.JTable();
        pbProgress = new javax.swing.JProgressBar();
        btnNom = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("NOM");
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setName("frmNom"); // NOI18N

        taInfo.setEditable(false);
        taInfo.setColumns(20);
        taInfo.setRows(5);
        spInfo.setViewportView(taInfo);

        tDownloads.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Download", "Type", "State", "Progress"
            }
        ) {
            Class[] types = new Class [] {
                String.class, String.class, String.class, Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tDownloads.setShowHorizontalLines(false);
        tDownloads.setShowVerticalLines(false);
        spDownloads.setViewportView(tDownloads);
        tDownloads.getColumn("Progress").setCellRenderer(new ProgressCellRender());

        pbProgress.setStringPainted(true);

        btnNom.setText("NOM!");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spDownloads, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
                    .addComponent(spInfo, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pbProgress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNom, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spDownloads, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnNom, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pbProgress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }

    private javax.swing.JButton btnNom;
    private javax.swing.JProgressBar pbProgress;
    private javax.swing.JScrollPane spDownloads;
    private javax.swing.JScrollPane spInfo;
    private javax.swing.JTable tDownloads;
    private javax.swing.JTextArea taInfo;

}
