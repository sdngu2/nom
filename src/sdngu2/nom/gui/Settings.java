package sdngu2.nom.gui;

public class Settings extends javax.swing.JFrame {

    public Settings() {
        initComponents();
    }

    private void initComponents() {

        lblUnits = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tUnits = new javax.swing.JTable();
        cbTotalDownloadCap = new javax.swing.JCheckBox();
        txtTotalDownloadCap = new javax.swing.JTextField();
        cbDownloadCapPerFile = new javax.swing.JCheckBox();
        txtDownloadCapPerFile = new javax.swing.JTextField();
        cbIgnoreExtensions = new javax.swing.JCheckBox();
        txtIgnoreExtensions = new javax.swing.JTextField();
        cbOpenFolderUponFinish = new javax.swing.JCheckBox();
        btnApply = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Settings");
        setResizable(false);

        lblUnits.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblUnits.setText("Units to download from");

        tUnits.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tUnits);

        cbTotalDownloadCap.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbTotalDownloadCap.setText("Total download cap (MB)");

        txtTotalDownloadCap.setText("1024");

        cbDownloadCapPerFile.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbDownloadCapPerFile.setText("Download cap per file (MB)");

        txtDownloadCapPerFile.setText("16");

        cbIgnoreExtensions.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbIgnoreExtensions.setText("Ignore extensions");

        txtIgnoreExtensions.setText("mp4,avi,m4v,exe");

        cbOpenFolderUponFinish.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cbOpenFolderUponFinish.setText("Open folder upon finish");

        btnApply.setText("Apply");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cbTotalDownloadCap)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtTotalDownloadCap, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cbDownloadCapPerFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtDownloadCapPerFile, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cbIgnoreExtensions)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtIgnoreExtensions, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblUnits)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cbOpenFolderUponFinish)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnApply, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblUnits)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbTotalDownloadCap)
                            .addComponent(txtTotalDownloadCap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbDownloadCapPerFile)
                            .addComponent(txtDownloadCapPerFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbIgnoreExtensions)
                            .addComponent(txtIgnoreExtensions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 45, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnApply, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbOpenFolderUponFinish))))
                .addContainerGap())
        );

        pack();
    }

    private javax.swing.JButton btnApply;
    private javax.swing.JCheckBox cbDownloadCapPerFile;
    private javax.swing.JCheckBox cbIgnoreExtensions;
    private javax.swing.JCheckBox cbOpenFolderUponFinish;
    private javax.swing.JCheckBox cbTotalDownloadCap;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblUnits;
    private javax.swing.JTable tUnits;
    private javax.swing.JTextField txtDownloadCapPerFile;
    private javax.swing.JTextField txtIgnoreExtensions;
    private javax.swing.JTextField txtTotalDownloadCap;

}
