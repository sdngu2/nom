package sdngu2.nom.gui.components;

import java.awt.Component;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class ProgressCellRender extends JProgressBar implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        int progress = 0;
        if (value instanceof Float) {
            progress = (int)Math.floor((Float) value * 100);
        } else if (value instanceof Integer) {
            progress = (Integer) value;
        }
        setValue(progress);
        return this;
    }
    
}
